import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HeroDetailComponent } from './hero-detail.component';
@NgModule({
    imports: [BrowserModule],
    declarations: [HeroDetailComponent],
    bootstrap: [HeroDetailComponent]
})
export class HeroDetailModule {}