import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        let heroes = [
                {id: 1, name: 'Anton'},
                {id: 2, name: 'Svatya'},
                {id: 3, name: 'Sergey'},
                {id: 4, name: 'Airat'},
                {id: 5, name: 'Edward'},
                {id: 6, name: 'Dima'},
                {id: 7, name: 'Marat'},
                {id: 8, name: 'Marina'},
                {id: 9, name: 'Tanya'},
                {id: 10, name: 'Dima'}
        ];
        return {heroes};
    }
}