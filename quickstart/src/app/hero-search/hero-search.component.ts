import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

import { Router } from '@angular/router';

import { HeroSearchService } from '../hero-service/hero-search.service';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

//observable class extensions
import 'rxjs/add/observable/of';

//observable operators
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/distinctUntilChanged';

import { Hero } from '../hero-service/hero';
import { HeroService } from '../hero-service/hero.service';

@Component ({
    selector: 'hero-search',
    templateUrl: './hero-search.component.html',
    styleUrls: ['./hero-search.component.css'],
    providers: [HeroSearchService]
})
export class HeroSearchComponent implements OnInit{
    heroes: Observable<Hero[]>;
    private searchTerms = new Subject<string>();

    constructor(
        private router: Router,
        private heroSearchService: HeroSearchService
    ) {}

    //кладем в наблюдателя получаемый массив 
    search(term: string) {
        this.searchTerms.next(term);
    }
    ngOnInit():void {
        this.heroes = this.searchTerms
            .debounceTime(300) //время ожидания обновления списка
            .distinctUntilChanged() //игнорируем элементы уже имеющиеся в обзерве
            .switchMap(term => 
            term 
            ? this.heroSearchService.search(term)
            : Observable.of<Hero[]>([]))
            .catch(error => {
                console.error(error);
                return Observable.of<Hero[]>([]);
            })
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        
    }
    goToDetails(hero: Hero): void {
        let url = ['/hero-detail', hero.id];
        this.router.navigate(url);
    }
}