import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HeroSearchComponent } from './hero-search.component';

@NgModule({
    declarations: [HeroSearchComponent],
    bootstrap: [HeroSearchComponent],
    imports: [BrowserModule]
})

export class HeroSearchModule {} 