"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var hero_search_service_1 = require("../hero-service/hero-search.service");
var Observable_1 = require("rxjs/Observable");
var Subject_1 = require("rxjs/Subject");
//observable class extensions
require("rxjs/add/observable/of");
//observable operators
require("rxjs/add/operator/debounceTime");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/distinctUntilChanged");
var HeroSearchComponent = (function () {
    function HeroSearchComponent(router, heroSearchService) {
        this.router = router;
        this.heroSearchService = heroSearchService;
        this.searchTerms = new Subject_1.Subject();
    }
    //кладем в наблюдателя получаемый массив 
    HeroSearchComponent.prototype.search = function (term) {
        this.searchTerms.next(term);
    };
    HeroSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.heroes = this.searchTerms
            .debounceTime(300) //время ожидания обновления списка
            .distinctUntilChanged() //игнорируем элементы уже имеющиеся в обзерве
            .switchMap(function (term) {
            return term
                ? _this.heroSearchService.search(term)
                : Observable_1.Observable.of([]);
        })
            .catch(function (error) {
            console.error(error);
            return Observable_1.Observable.of([]);
        });
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
    };
    HeroSearchComponent.prototype.goToDetails = function (hero) {
        var url = ['/hero-detail', hero.id];
        this.router.navigate(url);
    };
    return HeroSearchComponent;
}());
HeroSearchComponent = __decorate([
    core_1.Component({
        selector: 'hero-search',
        templateUrl: './hero-search.component.html',
        styleUrls: ['./hero-search.component.css'],
        providers: [hero_search_service_1.HeroSearchService]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        hero_search_service_1.HeroSearchService])
], HeroSearchComponent);
exports.HeroSearchComponent = HeroSearchComponent;
//# sourceMappingURL=hero-search.component.js.map