import { Component } from '@angular/core';
import { HeroService } from './hero-service/hero.service';

@Component ({
    selector: 'my-app',
    templateUrl: './app.template.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title:string = 'Tour of Heroes!';
}