import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroDashboardComponent }   from './hero-dashboard/hero-dashboard.component';
import { HeroesListComponent }      from './hero-list/heroes-list.component';
import { HeroDetailComponent }  from './hero-detail/hero-detail.component';
const routes: Routes = [
      {
        path: '',
        redirectTo: 'hero-dashboard',
        pathMatch: 'full'
      },
      {
        path: 'hero-list',
        component: HeroesListComponent
      },
      {
          path: 'hero-dashboard',
          component: HeroDashboardComponent
      },
      {
          path: 'hero-detail/:id',
          component: HeroDetailComponent
      }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}