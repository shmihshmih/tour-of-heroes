import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Hero } from './hero';

@Injectable()
export class HeroSearchService {
    constructor(private http: Http) {};

    search(heroName: string): Observable<Hero[]> {
        return this.http
                .get(`app/heroes/?name=${heroName}`)
                .map(response => response.json().data as Hero[]);
    }
}