import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Hero } from './hero';
//import { HEROES } from './hero.mock-module';

@Injectable()
export class HeroService{
    constructor(private http: Http) {}

    private heroesUrl = 'api/heroes';
    private headers = new Headers({'Content-Type': 'application/json'});

     getHeroes(): Promise<Hero[]> {
         //return Promise.resolve(HEROES);
         //it is ruturns us Observable we need toPromise to promise
         return this.http.get(this.heroesUrl)
            .toPromise()
            .then(responce => responce.json().data as Hero[])
            .catch(this.handleError);
     }
     getHero(id: number): Promise<Hero> {
         const url = `${this.heroesUrl}/${id}`;
         //return this.getHeroes()
         //  .then(heroes => heroes.find(hero => hero.id === id));
         return this.http.get(url)
            .toPromise()
            .then(responce => responce.json().data as Hero)
            .catch(this.handleError);

     }
     private handleError(error: any): Promise<any> {
        console.error('Sorry! We have problem!', error);
        return Promise.reject(error.message || error);
     }
     update(hero: Hero): Promise<Hero> {
         const url = `${this.heroesUrl}/${hero.id}`;
         return this.http
            .put(url, JSON.stringify(hero), {headers: this.headers})
            .toPromise()
            .then(() => hero)
            .catch(this.handleError);
     }
     create(value: string): Promise<Hero> {
         return this.http
            .post(this.heroesUrl, JSON.stringify({name: value}), {headers: new Headers})
            .toPromise()
            .then(res => res.json().data as Hero)
            .catch(this.handleError);
     }
     remove(id: number): Promise<void> {
         const url = `${this.heroesUrl}/${id}`;
         return this.http
            .delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
     }
};