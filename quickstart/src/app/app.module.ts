import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { HeroService }         from './hero-service/hero.service';
import { AppComponent } from './app.component';
import { HeroesListComponent } from './hero-list/heroes-list.component';
import { HeroDashboardComponent } from './hero-dashboard/hero-dashboard.component';

import { AppRoutingModule } from './app-routing.module';

import { HttpModule } from '@angular/http';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

import { HeroSearchComponent } from './hero-search/hero-search.component';

@NgModule({
    imports: [
      BrowserModule, 
      FormsModule,
      AppRoutingModule,
      HttpModule,
      InMemoryWebApiModule.forRoot(InMemoryDataService)
    ],
    declarations: [AppComponent, HeroesListComponent, HeroDetailComponent, HeroDashboardComponent, HeroSearchComponent],
    bootstrap: [AppComponent],
    providers: [HeroService]
})

export class AppModule {}