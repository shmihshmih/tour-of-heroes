import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HeroDashboardComponent } from './hero-dashboard.component';

@NgModule ({
    imports: [BrowserModule],
    declarations: [HeroDashboardComponent],
    bootstrap: [HeroDashboardComponent]
})
export class HeroDashboard {}