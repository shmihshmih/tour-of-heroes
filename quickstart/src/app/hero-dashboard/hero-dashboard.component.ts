import { Component, OnInit } from '@angular/core';
import { HeroService } from '../hero-service/hero.service';
import { Hero } from '../hero-service/hero';

@Component({
  selector: 'hero-dashboard',
  templateUrl: './hero-dashboard.component.html',
  styleUrls: ['./hero-dashboard.component.css']
})
export class HeroDashboardComponent implements OnInit{
    heroes: Hero[] = [];
    constructor(private heroService: HeroService) {}
    ngOnInit(): void {
        this.heroService.getHeroes().then(heroes => this.heroes = heroes.slice(1,5))
    }
}