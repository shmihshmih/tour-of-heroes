import { Component } from '@angular/core';
import { Hero } from '../hero-service/hero';
import { HeroService } from '../hero-service/hero.service';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'heroes-list',
  templateUrl: `./heroes-list.template.html`,
  styleUrls: ['./heroes-list.component.css']
})
export class HeroesListComponent implements OnInit {
  
  ngOnInit(): void {
    this.getHeroes();
  }

    constructor(
    private router: Router,
    private heroService: HeroService) { }

  heroes: Hero[];

  getHeroes():void {
    this.heroService.getHeroes().then(heroes => this.heroes = heroes);
  }
  title = 'Tour of Heroes!';

  selectedHero: Hero;

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  gotoDetail(): void {
  this.router.navigate(['/hero-detail', this.selectedHero.id]);
  }
  add(value: string): void{
    value = value.trim();
    if(!value) {return;};
    this.heroService.create(value).
      then(hero => {
          this.heroes.push(hero);
          this.selectedHero = null;
      })
  }
  delete(hero: Hero): void {
    this.heroService
      .remove(hero.id)
      .then(() => {
        this.heroes = this.heroes.filter(h => h !== hero);
        if(this.selectedHero === hero) {this.selectedHero = null};
      })
  }
}
