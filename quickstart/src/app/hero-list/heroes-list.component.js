"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var hero_service_1 = require("../hero-service/hero.service");
var router_1 = require("@angular/router");
var HeroesListComponent = (function () {
    function HeroesListComponent(router, heroService) {
        this.router = router;
        this.heroService = heroService;
        this.title = 'Tour of Heroes!';
    }
    HeroesListComponent.prototype.ngOnInit = function () {
        this.getHeroes();
    };
    HeroesListComponent.prototype.getHeroes = function () {
        var _this = this;
        this.heroService.getHeroes().then(function (heroes) { return _this.heroes = heroes; });
    };
    HeroesListComponent.prototype.onSelect = function (hero) {
        this.selectedHero = hero;
    };
    HeroesListComponent.prototype.gotoDetail = function () {
        this.router.navigate(['/hero-detail', this.selectedHero.id]);
    };
    HeroesListComponent.prototype.add = function (value) {
        var _this = this;
        value = value.trim();
        if (!value) {
            return;
        }
        ;
        this.heroService.create(value).
            then(function (hero) {
            _this.heroes.push(hero);
            _this.selectedHero = null;
        });
    };
    HeroesListComponent.prototype.delete = function (hero) {
        var _this = this;
        this.heroService
            .remove(hero.id)
            .then(function () {
            _this.heroes = _this.heroes.filter(function (h) { return h !== hero; });
            if (_this.selectedHero === hero) {
                _this.selectedHero = null;
            }
            ;
        });
    };
    return HeroesListComponent;
}());
HeroesListComponent = __decorate([
    core_1.Component({
        selector: 'heroes-list',
        templateUrl: "./heroes-list.template.html",
        styleUrls: ['./heroes-list.component.css']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        hero_service_1.HeroService])
], HeroesListComponent);
exports.HeroesListComponent = HeroesListComponent;
//# sourceMappingURL=heroes-list.component.js.map