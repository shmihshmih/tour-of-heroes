import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { HeroesListComponent }  from './heroes-list.component';
import { HeroDetailComponent } from '../hero-detail/hero-detail.component';



@NgModule({
  imports:      [ 
    BrowserModule, 
    FormsModule
   ],
  declarations: [ 
    HeroesListComponent,
    HeroDetailComponent
   ],
  bootstrap:    [ HeroesListComponent ]
})


export class AppModule { }
